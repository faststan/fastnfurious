""" A dummy conftest to illustrate pytest configuration """

from pytest import fixture

from fastnfurious import __version__


@fixture
def version() -> str:
    """ Return the current version of the fastnfurious package """
    return __version__
